<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Example extends CI_Controller {

	public function index() {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
			if (!$this->db->table_exists($this->uri->segment(2))){
				$this->base_m->create_table($this->uri->segment(2));
			}
            // DEFAULT DATA
			$data = loadDefaultData();
			if($this->session->userdata('lang') != 'pl'){
				if($this->db->table_exists($this->session->userdata('lang').'_'.$this->uri->segment(2))){
					$data['rows'] = $this->back_m->get_all($this->session->userdata('lang').'_'.$this->uri->segment(2));	
				}else{
					$this->session->set_flashdata('flashdata','Proszę stworzyć tabelę '.$this->session->userdata('lang').'_'.$this->uri->segment(2).'!');
					redirect('panel/jezyki');
				}
				
			}else $data['rows'] = $this->back_m->get_all($this->uri->segment(2));
			echo loadSubViewsBack($this->uri->segment(2), 'index', $data);
		} else {
			redirect('panel');
		}
	}

	public function form($type, $id = '') {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
            // DEFAULT DATA
			$data = loadDefaultData();

            if($id != '') {
            	if($this->session->userdata('lang') != 'pl'){
					$data['value'] = $this->back_m->get_one($this->session->userdata('lang').'_'.$this->uri->segment(2),$id);
				}else $data['value'] = $this->back_m->get_one($this->uri->segment(2), $id);
			    
            }
			echo loadSubViewsBack($this->uri->segment(2), $type, $data);
		} else {
			redirect('panel');
		}
	} 

	public function action($type, $table, $id = '') {
		if(checkAccess($access_group = ['administrator', 'redaktor'], $_SESSION['rola'])) {
			$now = date('Y-m-d');
			if (!is_dir('uploads/'.$now)) {
				mkdir('./uploads/' . $now, 0777, TRUE);
			}
			$config['upload_path'] = './uploads/'.$now;
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = 0;
			$config['max_width'] = 0;
			$config['max_height'] = 0;
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			
			foreach ($_POST as $key => $value) {

				if (!$this->db->field_exists($key, $table)) {
					$this->base_m->create_column($table, $key);
				}

				if($key == 'name_photo_1') {
					if ($this->upload->do_upload('photo_1')) {
						$data = $this->upload->data();
						$insert['photo'] = $now.'/'.$data['file_name'];  
						convert__to__webp($insert['photo']); 
						addMedia($data);
					} elseif($value == 'usunięte') {
						$insert['photo'] = '';
					}
				} else if($key == 'name_photo_2') {
					if ($this->upload->do_upload('photo_2')) {
						$data = $this->upload->data();
						$insert['photo2'] = $now.'/'.$data['file_name'];  
						convert__to__webp($insert['photo2']); 
						addMedia($data);
					} elseif($value == 'usunięte') {
						$insert['photo2'] = '';
					}
				}else if($key == 'server_photo_1'){
					if($value != ''){
						$insert['photo'] = $value;
					}
					if($value == 'usunięte'){
						$insert['photo'] = '';
					}
					
				}else if($key == 'server_photo_2'){
					if($value != ''){
						$insert['photo2'] = $value;
					}
					if($value == 'usunięte'){
						$insert['photo2'] = '';
					}
					
				} else {
					$insert[$key] = $value; 
				}
            }
            if($type == 'insert') {
            	if($this->session->userdata('lang')!= 'pl'){
            		$this->back_m->insert($this->session->userdata('lang').'_'.$table, $insert);
            	}else $this->back_m->insert($table, $insert);
			    
			    $this->session->set_flashdata('flashdata', 'Rekord został dodany!');
            } else {
            	if($this->session->userdata('lang')!= 'pl'){
            		$this->back_m->update($this->session->userdata('lang').'_'.$table, $insert, $id);
            	}else $this->back_m->update($table, $insert, $id);
			    
			    $this->session->set_flashdata('flashdata', 'Rekord został zaktualizowany!');   
            }
			redirect('panel/'.$this->uri->segment(2));
		} else {
			redirect('panel');
		}
    }
}