<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (!$this->db->table_exists('users')){
			$this->base_m->create_base();
		}
		if($this->session->userdata('lang') == ''){
			$this->session->set_userdata('lang','pl');
		}
	}

	public function index() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			if($this->db->table_exists($this->session->userdata('lang').'_slider')){
				$data['slider'] = $this->back_m->get_all($this->session->userdata('lang').'_slider');
				
			}else{
				$data['slider'] = $this->back_m->get_all('slider');
				
			}
			if($this->db->table_exists($this->session->userdata('lang').'_about')){
				$data['about'] = $this->back_m->get_all($this->session->userdata('lang').'_about');
				
			}else{
				$data['about'] = $this->back_m->get_all('about');
				
			}
			if($this->db->table_exists($this->session->userdata('lang').'_parallax')){
				$data['parallax'] = $this->back_m->get_one($this->session->userdata('lang').'_parallax',1);
				
			}else{
				$data['parallax'] = $this->back_m->get_one('parallax',1);
				
			}
			if($this->db->table_exists($this->session->userdata('lang').'_atributes')){
				$data['atributes'] = $this->back_m->get_with_limit($this->session->userdata('lang').'_atributes',2);
				
			}else{
				$data['atributes'] = $this->back_m->get_with_limit('atributes',2);
				
			}
			if($this->db->table_exists($this->session->userdata('lang').'_offer')){
				$data['offer'] = $this->back_m->get_with_limit($this->session->userdata('lang').'_offer',3);
				
			}else{
				$data['offer'] = $this->back_m->get_with_limit('offer',3);
				
			}
				
				
					
		}else{
			$data['slider'] = $this->back_m->get_all('slider');
			$data['about'] = $this->back_m->get_one('about',1);
			$data['parallax'] = $this->back_m->get_one('parallax',1);
			$data['atributes'] = $this->back_m->get_with_limit('atributes',2);
			$data['offer'] = $this->back_m->get_with_limit('offer',3);	
		}
		
		echo loadViewsFront('index',$data);
	}


	public function about() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			if($this->db->table_exists($this->session->userdata('lang').'_about')){
				$data['about'] = $this->back_m->get_one($this->session->userdata('lang').'_about',1);
				$data['history'] = $this->back_m->get_all($this->session->userdata('lang').'_historia');
				$data['srodki'] = $this->back_m->get_one($this->session->userdata('lang').'_srodki',1);
			}else{
				$data['about'] = $this->back_m->get_one('about',1);
				$data['history'] = $this->back_m->get_all('historia');
				$data['srodki'] = $this->back_m->get_one('srodki',1);
			}
			
		}else{
			$data['about'] = $this->back_m->get_one('about',1);
			$data['history'] = $this->back_m->get_all('historia');
			$data['srodki'] = $this->back_m->get_one('srodki',1);
		}
		
		echo loadViewsFront('about',$data);
	}

	public function srodki() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			if($this->db->table_exists($this->session->userdata('lang').'_srodki')){
				$data['about'] = $this->back_m->get_one($this->session->userdata('lang').'_srodki',1);
			}else{
				$data['about'] = $this->back_m->get_one('srodki',1);
			}
			
		}else{
			$data['about'] = $this->back_m->get_one('srodki',1);
		}
		
		echo loadViewsFront('srodki',$data);
	}

	public function history() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			if($this->db->table_exists($this->session->userdata('lang').'_historia')){
				$data['about'] = $this->back_m->get_all($this->session->userdata('lang').'_historia');
			}else{
				$data['about'] = $this->back_m->get_all('historia');
			}
			
		}else{
			$data['about'] = $this->back_m->get_all('historia');
		}
		
		echo loadViewsFront('history',$data);
	}


	public function blog() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			$data['blog'] = $this->back_m->get_all($this->session->userdata('lang').'_blog');
		}else{
			$data['blog'] = $this->back_m->get_all('blog');
		}

		echo loadViewsFront('blog',$data);
	}

	public function projects() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			$data['blog'] = $this->back_m->get_all($this->session->userdata('lang').'_realizacje');
		}else{
			$data['blog'] = $this->back_m->get_all('realizacje');
		}

		echo loadViewsFront('projects',$data);
	}

	public function single_project($id,$slug) {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			$data['single'] = $this->back_m->get_one($this->session->userdata('lang').'_realizacje',$id);
			$data['blog'] = $this->back_m->get_with_limit($this->session->userdata('lang').'_realizacje',5,'desc');
			$data['gallery'] = $this->back_m->get_images('gallery',$this->session->userdata('lang').'_realizacje',$id);
		}else{
			$data['single'] = $this->back_m->get_one('realizacje',$id);
			$data['blog'] = $this->back_m->get_with_limit('realizacje',5,'desc');
			$data['gallery'] = $this->back_m->get_images('gallery','realizacje',$id);
		}

		echo loadViewsFront('single_project',$data);
	}

	public function single_article($id,$slug) {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			$data['single'] = $this->back_m->get_one($this->session->userdata('lang').'_blog',$id);
			$data['blog'] = $this->back_m->get_with_limit($this->session->userdata('lang').'_blog',5,'desc');
			$data['gallery'] = $this->back_m->get_images('gallery',$this->session->userdata('lang').'_blog',$id);
		}else{
			$data['single'] = $this->back_m->get_one('blog',$id);
			$data['blog'] = $this->back_m->get_with_limit('blog',5,'desc');
			$data['gallery'] = $this->back_m->get_images('gallery','blog',$id);
		}

		echo loadViewsFront('single_article',$data);
	}


	public function offer() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			$data['offer'] = $this->back_m->get_all($this->session->userdata('lang').'_offer');
		}else{
			$data['offer'] = $this->back_m->get_all('offer');
		}
		
		echo loadViewsFront('offer',$data);
	}


	public function single_offer($id,$slug) {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			$data['single'] = $this->back_m->get_one($this->session->userdata('lang').'_offer',$id);
			$data['gallery'] = $this->back_m->get_images('gallery',$this->session->userdata('lang').'_offer',$id);
		}else{
			$data['single'] = $this->back_m->get_one('offer',$id);
			$data['gallery'] = $this->back_m->get_images('gallery','offer',$id);
		}
		
		echo loadViewsFront('single_offer',$data);
	}


	public function gallery() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			$data['gallery'] = $this->back_m->get_images('gallery',$this->session->userdata('lang').'_gallery_page',1);
		}else{
			$data['gallery'] = $this->back_m->get_images('gallery','gallery_page',1);
		}
		
		echo loadViewsFront('gallery',$data);
	}

	public function job(){
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			if($this->db->table_exists($this->session->userdata('lang').'_praca')){
				$data['about'] = $this->back_m->get_one($this->session->userdata('lang').'_praca',1);
			}else{
				$data['about'] = $this->back_m->get_one('praca',1);
			}
			
		}else{
			$data['about'] = $this->back_m->get_one('praca',1);
		}

		echo loadViewsFront('job', $data);
	}


	public function contact() {
		$data = loadDefaultDataFront();
		if($this->session->userdata('lang') != 'pl'){
			if($this->db->table_exists($this->session->userdata('lang').'_praca')){
				$data['kontakt'] = $this->back_m->get_all($this->session->userdata('lang').'_kontakt');
				$data['dane_spolki'] = $this->back_m->get_one($this->session->userdata('lang').'_dane_spolki',1);
			}else{
				$data['kontakt'] = $this->back_m->get_all('kontakt');
				$data['dane_spolki'] = $this->back_m->get_one('dane_spolki',1);
			}
			
		}else{
			$data['kontakt'] = $this->back_m->get_all('kontakt');
			$data['dane_spolki'] = $this->back_m->get_one('dane_spolki',1);
		}
		
		
		echo loadViewsFront('contact',$data);
	}

	public function set_lang(){
		$this->session->set_userdata('lang', $_POST['lang']);
		redirectAfterLangChange($_POST['redirect']);
	}
}