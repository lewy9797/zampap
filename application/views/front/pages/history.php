
  <div class="container mt-4 mb-3">
    <div class="row">
      <div class="col-md-12 d-inline">
        <h4 class="text-uppercase"><a href="<?= base_url() ?>" class="first-color home-text font-weight-normal">HOME</a> > <span class="font-weight-bold second-color"><?= $current_page->title; ?></span></h4>
      </div>
    </div>
  </div>
  <div class="container">
	<?php foreach($about as $a): ?>
    <section class="my-5 top_margin">

      <div class="row">
        <div class="col-md-6 mt-md-0 mt-3 text-left p-0 about__bg">
          <picture>
            <source srcset="<?= base_url().'uploads/'.$a->photo ?>.webp" type="image/webp" class="img-fluid">
            <source srcset="<?= base_url().'uploads/'.$a->photo ?>" type="image/jpeg" class="img-fluid"> 
            <img src="<?= base_url().'uploads/'.$a->photo ?>" alt="<?= $a->alt ?>" class="img-fluid">
          </picture>
        </div>
        <div class="col-md-6 mt-md-0 mt-3 px-5 text-left d-flex align-items-center">
          <div>
            <h3 class="font-weight-bold"><?= $a->title; ?></h3>
            <?= $a->subtitle; ?>
          </div>
        </div>
        

      </div>
  
    </section>
<?php endforeach; ?>
  </div>