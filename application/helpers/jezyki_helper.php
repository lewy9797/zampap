<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    function fillRecords($insert){
        fillAbout($insert);
        fillAtributes($insert);
        fillBlog($insert);
        fillContactSettings($insert);
        fillKontakt($insert);
        fillNaglowki($insert);
        fillOffer($insert);
        fillParallax($insert);
        fillSettings($insert);
        fillSlider($insert);
        fillSubpages($insert);
        fillHistory($insert);
        fillPraca($insert);
        fillRealizacje($insert);
        fillSrodki($insert);
        fillDaneSpolki($insert);
    }

    function fillAbout($insert){
        $CI = &get_instance();
        if(isset($insert['tables'])){
            $about_data = $CI->back_m->get_all('about');
            foreach($about_data as $about){
                $about_insert = array(
                    'title' => $about->title,
                    'photo' => $about->photo,
                    'alt' => $about->alt,
                    'description' => $about->description,
                    'short_description' => $about->short_description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_about')) < count($about_data)){
                    $CI->back_m->insert($insert['prefix'].'_about', $about_insert);
                }
            }
            
        }
    }
    function fillAtributes($insert){
        $CI = &get_instance();
        if(isset($insert['atributes'])){
            $atributes_data = $CI->back_m->get_all('atributes');
            foreach($atributes_data as $atributes){
                $atributes_insert = array(
                    'title' => $atributes->title,
                    'subtitle' => $atributes->subtitle,
                    'photo' => $atributes->photo,
                    'alt' => $atributes->alt,
                    'description' => $atributes->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_atributes')) < count($atributes_data)){
                    $CI->back_m->insert($insert['prefix'].'_atributes', $atributes_insert);
                }
            }
        }
    }
    function fillSrodki($insert){
        $CI = &get_instance();
        if(isset($insert['srodki'])){
            $srodki_data = $CI->back_m->get_all('srodki');
            foreach($srodki_data as $srodki){
                $srodki_insert = array(
                    'title' => $srodki->title,
                    'subtitle' => $srodki->subtitle,
                    'photo' => $srodki->photo,
                    'alt' => $srodki->alt,
                    'description' => $srodki->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_srodki')) < count($srodki_data)){
                    $CI->back_m->insert($insert['prefix'].'_srodki', $srodki_insert);
                }
            }
        }
    }

    function fillDaneSpolki($insert){
        $CI = &get_instance();
        if(isset($insert['dane_spolki'])){
            $dane_spolki_data = $CI->back_m->get_all('dane_spolki');
            foreach($dane_spolki_data as $dane_spolki){
                $dane_spolki_insert = array(
                    'title' => $dane_spolki->title,
                    'subtitle' => $dane_spolki->subtitle,
                    'photo' => $dane_spolki->photo,
                    'alt' => $dane_spolki->alt,
                    'description' => $dane_spolki->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_dane_spolki')) < count($dane_spolki_data)){
                    $CI->back_m->insert($insert['prefix'].'_dane_spolki', $dane_spolki_insert);
                }
            }
        }
    }

    function fillHistory($insert){
        $CI = &get_instance();
        if(isset($insert['historia'])){
            $historia_data = $CI->back_m->get_all('historia');
            foreach($historia_data as $historia){
                $historia_insert = array(
                    'title' => $historia->title,
                    'subtitle' => $historia->subtitle,
                    'photo' => $historia->photo,
                    'alt' => $historia->alt,
                    'description' => $historia->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_historia')) < count($historia_data)){
                    $CI->back_m->insert($insert['prefix'].'_historia', $historia_insert);
                }
            }
        }
    }

    function fillPraca($insert){
        $CI = &get_instance();
        if(isset($insert['praca'])){
            $praca_data = $CI->back_m->get_all('praca');
            foreach($praca_data as $praca){
                $praca_insert = array(
                    'title' => $praca->title,
                    'subtitle' => $praca->subtitle,
                    'photo' => $praca->photo,
                    'alt' => $praca->alt,
                    'description' => $praca->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_praca')) < count($praca_data)){
                    $CI->back_m->insert($insert['prefix'].'_praca', $praca_insert);
                }
            }
        }
    }

    function fillBlog($insert){
        $CI = &get_instance();
        if(isset($insert['blog'])){
            $blog_data = $CI->back_m->get_all('blog');            
            foreach($blog_data as $blog){
                $blog_insert = array(
                    'title' => $blog->title,
                    'photo' => $blog->photo,
                    'alt' => $blog->alt,
                    'alt2' => $blog->alt2,
                    'photo2' => $blog->photo2,
                    'description' => $blog->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_blog')) < count($blog_data)){
                    $CI->back_m->insert($insert['prefix'].'_blog', $blog_insert);
                }
            }
        }
    }

    function fillRealizacje($insert){
        $CI = &get_instance();
        if(isset($insert['realizacje'])){
            $realizacje_data = $CI->back_m->get_all('realizacje');            
            foreach($realizacje_data as $realizacje){
                $realizacje_insert = array(
                    'title' => $realizacje->title,
                    'photo' => $realizacje->photo,
                    'alt' => $realizacje->alt,
                    'alt2' => $realizacje->alt2,
                    'photo2' => $realizacje->photo2,
                    'description' => $realizacje->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_realizacje')) < count($realizacje_data)){
                    $CI->back_m->insert($insert['prefix'].'_realizacje', $realizacje_insert);
                }
            }
        }
    }

    function fillSubpages($insert){
        $CI = &get_instance();
        if(isset($insert['subpages'])){
            $subpages_data = $CI->back_m->get_all('subpages');
            foreach($subpages_data as $subpages){
                $subpage_insert = array(
                    'title' => $subpages->title,
                    'subtitle' => $subpages->subtitle,
                    'page' => $subpages->page,
                    'table_name' => $subpages->table_name
                );
                if (!$CI->db->field_exists('table_name', $insert['prefix'].'_subpages' )) {
                    $CI->base_m->create_column($insert['prefix'].'_subpages', 'table_name');
                }
                if(count($CI->back_m->get_all($insert['prefix'].'_subpages')) < count($subpages_data)){
                    $CI->back_m->insert($insert['prefix'].'_subpages', $subpage_insert);
                }
            }   
        }
    }


    function fillParallax($insert){
        $CI = &get_instance();
        if(isset($insert['parallax'])){
            $parallax_data = $CI->back_m->get_all('parallax');
            foreach($parallax_data as $parallax){
                $parallax_insert = array(
                    'title' => $parallax->title,
                    'subtitle' => $parallax->subtitle,
                    'alt' => $parallax->alt,
                    'photo' => $parallax->photo,
                    'description' => $parallax->description,
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_parallax')) < count($parallax_data)){
                    $CI->back_m->insert($insert['prefix'].'_parallax', $parallax_insert);
                }
            }
            
        }
    }

    function fillSlider($insert){
        $CI = &get_instance();
        if(isset($insert['slider'])){
            $slider_data = $CI->back_m->get_all('slider');
           foreach($slider_data as $slider){
                $slider_insert = array(
                    'title' => $slider->title,
                    'subtitle' => $slider->subtitle,
                    'photo' => $slider->photo,
                    'link' => $slider->link,
                    'button' => $slider->button
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_slider')) < count($slider_data)){
                    $CI->back_m->insert($insert['prefix'].'_slider', $slider_insert);
                }
            }
        }
    }


    function fillNaglowki($insert){
        $CI = &get_instance();
        if(isset($insert['naglowki'])){
            $naglowki_data = $CI->back_m->get_all('naglowki');
            foreach($naglowki_data as $naglowki){
                $naglowki_insert = array(
                    'title' => $naglowki->title,
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_naglowki')) < count($naglowki_data)){
                    $CI->back_m->insert($insert['prefix'].'_naglowki', $naglowki_insert);
                }
            }
        }
    }

    function fillKontakt($insert){
        $CI = &get_instance();
        if(isset($insert['kontakt'])){
            $kontakt_data = $CI->back_m->get_all('kontakt');
            foreach($kontakt_data as $kontakt){
                $kontakt_insert = array(
                    'title' => $kontakt->title,
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_kontakt')) < count($kontakt_data)){
                    $CI->back_m->insert($insert['prefix'].'_kontakt', $kontakt_insert);
                }
            }
        }
    }

    function fillOffer($insert){
        $CI = &get_instance();
        if(isset($insert['offer'])){
            $offer_data = $CI->back_m->get_all('offer');
            foreach($offer_data as $offer){
                $offer_insert = array(
                    'title' => $offer->title,
                    'photo' => $offer->photo,
                    'alt' => $offer->alt,
                    'photo2' => $offer->photo2,
                    'alt2' => $offer->alt2,
                    'short_description' => $offer->short_description,
                    'description' => $offer->description
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_offer')) < count($offer_data)){
                    $CI->back_m->insert($insert['prefix'].'_offer', $offer_insert);
                }
            }
        }
    }

    



    function fillSettings($insert){
        $CI = &get_instance();
        if(isset($insert['settings'])){
            $settings_data = $CI->back_m->get_all('settings');
            foreach($settings_data as $settings){
                $settings_insert = array(
                    'first_color' => $settings->first_color,
                    'second_color' => $settings->second_color,
                    'meta_title' => $settings->meta_title,
                    'description' => $settings->description,
                    'keywords' => $settings->keywords,
                    'privace' => $settings->privace,
                    'logo' => $settings->logo,
                    'fb_link' => $settings->fb_link,
                    'inst_link' => $settings->inst_link,
                    'yt_link' => $settings->yt_link,
                    'tw_link' => $settings->tw_link,
                    'rodo' => $settings->rodo,
                    'rodo_tel' => $settings->rodo_tel,
                    'rodo_mail' => $settings->rodo_mail,
                    'captcha' => $settings->captcha,
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_settings')) < count($settings_data)){
                    $CI->back_m->insert($insert['prefix'].'_settings', $settings_insert);
                }
            }
            
        }
        
    }

    function fillContactSettings($insert){
        $CI = &get_instance();
        if(isset($insert['contact_settings'])){
            $contact_settings_data = $CI->back_m->get_all('contact_settings');
            foreach($contact_settings_data as $contact){
                $contact_settings_insert = array(
                    'company' => $contact->company,
                    'name' => $contact->name,
                    'map' => $contact->map,
                    'address' => $contact->address,
                    'city' => $contact->city,
                    'zip_code' => $contact->zip_code,
                    'phone1' => $contact->phone1,
                    'phone2' => $contact->phone2,
                    'email1' => $contact->email1,
                    'email2' => $contact->email2,
                    'label1' => $contact->label1,
                    'label2' => $contact->label2,
                    'label3' => $contact->label3,
                    'label4' => $contact->label4,
                    'label5' => $contact->label5,
                    'hours' => $contact->hours
                );
                if(count($CI->back_m->get_all($insert['prefix'].'_contact_settings')) < count($contact_settings_data)){
                    $CI->back_m->insert($insert['prefix'].'_contact_settings', $contact_settings_insert);
                }
            }
            
        }
        
    }