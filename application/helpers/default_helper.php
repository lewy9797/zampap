<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function loadDefaultData() {
    $CI = &get_instance();
    if($CI->session->userdata('lang') != 'pl'){
        if($CI->db->table_exists($CI->session->userdata('lang').'_settings')){
            $data['settings'] = $CI->back_m->get_one($CI->session->userdata('lang').'_settings', 1);
        }else $data['settings'] = $CI->back_m->get_one('settings', 1);
        
        if($CI->db->table_exists($CI->session->userdata('lang').'_contact_settings')){
            $data['contact'] = $CI->back_m->get_one($CI->session->userdata('lang').'_contact_settings', 1);
        }else $data['contact'] = $CI->back_m->get_one('contact_settings', 1);
        
    }else{
        $data['contact'] = $CI->back_m->get_one('contact_settings', 1);
        $data['settings'] = $CI->back_m->get_one('settings', 1);
    }
	$data['user'] = $CI->back_m->get_one('users', $_SESSION['id']);
    $data['mails'] = $CI->back_m->get_all('mails');
    $data['media'] = $CI->back_m->get_all('media');
    $data['jezyki'] = $CI->back_m->get_all('jezyki');
	
    return $data;
}

function loadDefaultDataFront() {
    $CI = &get_instance();
    if($CI->session->userdata('lang') != 'pl'){
    	if($CI->db->table_exists($CI->session->userdata('lang').'_offer')){
    		$data['oferta'] = $CI->back_m->get_with_limit($CI->session->userdata('lang').'_offer',4);
    	} else{
    		$data['oferta'] = $CI->back_m->get_with_limit('offer',4);
    	}

    	if($CI->db->table_exists($CI->session->userdata('lang').'_settings')){
    		$data['settings'] = $CI->back_m->get_one($CI->session->userdata('lang').'_settings', 1);
    	} else{
    		$data['settings'] = $CI->back_m->get_one('settings', 1);
    	}

    	if($CI->db->table_exists($CI->session->userdata('lang').'_subpages')){
    		$data['subpages'] = $CI->back_m->get_all($CI->session->userdata('lang').'_subpages');
			$data['footer_links'] = $CI->back_m->get_with_limit($CI->session->userdata('lang').'_subpages',4);
    	} else{
    		$data['subpages'] = $CI->back_m->get_all('subpages');
			$data['footer_links'] = $CI->back_m->get_with_limit('subpages',4);
    	}
    	
    	if($CI->db->table_exists($CI->session->userdata('lang').'_contact_settings')){
    		$data['contact'] = $CI->back_m->get_one($CI->session->userdata('lang').'_contact_settings', 1);
    	} else{
    		$data['contact'] = $CI->back_m->get_one('contact_settings', 1);
    	}
        if($CI->db->table_exists($CI->session->userdata('lang').'_naglowki')){
            $data['naglowki'] = $CI->back_m->get_all($CI->session->userdata('lang').'_naglowki');
        } else{
            $data['naglowki'] = $CI->back_m->get_all('naglowki');
        }
    	
		
		
		
    }else{
	    $data['settings'] = $CI->back_m->get_one('settings', 1);
		$data['subpages'] = $CI->back_m->get_all('subpages');
		$data['footer_links'] = $CI->back_m->get_with_limit('subpages',4);
		$data['contact'] = $CI->back_m->get_one('contact_settings', 1);
		$data['oferta'] = $CI->back_m->get_with_limit('offer',4);
        $data['naglowki'] = $CI->back_m->get_all('naglowki');

    }
    $data['jezyki'] = $CI->back_m->get_all('jezyki');
    $data['current_page'] = $CI->back_m->get_subpage($CI->uri->segment(1));

    return $data;
}

function assets(){
	return base_url().'assets/front/';
}


function images(){
	return base_url().'uploads/';
}

 
